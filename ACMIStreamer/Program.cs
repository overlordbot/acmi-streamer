﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ACMIStreamer
{
    class Program
    {
        private static TcpListener server;

        static void Main(string[] args)
        {
            server = new TcpListener(IPAddress.Parse("192.168.1.25"), 42674);
            server.Start();

            while(true)
            {
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine("Client Connected");
                NetworkStream stream = client.GetStream();
                StreamReader streamReader = new StreamReader(stream);
                StreamWriter streamWriter = new StreamWriter(stream);

                Console.WriteLine("Sending Handshake");
                streamWriter.Write("XtraLib.Stream.0\nTacview.RealTimeTelemetry.0\nACMIStreamer\n0\0");
                streamWriter.Flush();

                Console.WriteLine("Receiving Handshake");
                String streamProtocol = streamReader.ReadLine();
                Console.WriteLine(streamProtocol);
                if(streamProtocol != "XtraLib.Stream.0")
                {
                    Console.WriteLine("Stream Protocol not 'XtraLib.Stream.0'");
                    Console.WriteLine("Closing Connection");
                    stream.Close();
                    break;
                }

                String tacviewProtocol = streamReader.ReadLine();
                Console.WriteLine(tacviewProtocol);
                if (tacviewProtocol != "Tacview.RealTimeTelemetry.0")
                {
                    Console.WriteLine("Stream Protocol not 'Tacview.RealTimeTelemetry.0'");
                    Console.WriteLine("Closing Connection");
                    stream.Close();
                    break;
                }

                Console.WriteLine(streamReader.ReadLine());
                streamReader.Read(); // Read the solo carriage return.

                StreamReader fileReader = OpenTacviewFile();
                string line;
                try
                {
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                        streamWriter.WriteLine(line);
                    };
                    fileReader.Close();
                    client.Dispose();
                } catch(IOException e)
                {
                    fileReader.Close();
                    client.Dispose();
                }
            }
        }

        static private StreamReader OpenTacviewFile()
        {
            FileStream fileStream = File.Open("Tacview.txt.acmi", FileMode.Open);
            return new StreamReader(fileStream);
        }
    }
}